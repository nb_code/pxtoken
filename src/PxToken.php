<?php
namespace  CqPx;
class PxToken
{
    private $client_id="";
    private $host="";
    private $timeout=30;
    private $secret="";
    function __construct($host,$client_id,$secret)
    {
        $this->client_id=$client_id;
        $this->host=$host;
        $this->secret=$secret;
    }

    function verify_base_parms() : bool {
        if(empty($this->client_id) || empty($this->host)|| empty($this->secret))
            throw new \Exception("参数错误");
        return true;
    }

    function do_login($username,$password,$device="")  {
        $this->verify_base_parms();
        $headers = array(
            'client-id: '.$this->client_id
        ); 
        $data = array(
            'username' => $username,
            'password' => $password,
            'device'   => $device
        ); 
       
        $url=$this->host . '/oauth2/doLogin';
        try {
            $rest_str=$this->_post_form($url,$data,$headers);
            $rest=json_decode($rest_str,true);
            return $rest;
        } catch (\Exception $ex) {
            //throw $th;
            throw $ex;
        }
        return null;
    }

    function build_login_authorize($redirect_uri,$response_type="code") : string {

        if(empty($redirect_uri))
        throw new \Exception("redirect_uri 不能为空");

        $this->verify_base_parms();
        $params = array(
            'client_id' => $this->client_id,
            'redirect_uri' => $redirect_uri,
            'response_type'=>$response_type
        ); 
        $queryString = http_build_query($params);
        $signature=hash('sha256', $queryString.$this->secret);
        $queryString=$queryString."&signature=".$signature;
        return $this->host . '/oauth2/login_authorize?' . $queryString ;
    }

    function get_login_access_token($code)  {
        $this->verify_base_parms();
        $params = array(
            'client_id' => $this->client_id,
            'code' => $code
        ); 
        $queryString = http_build_query($params);
        $signature=hash('sha256', $queryString.$this->secret);
        $queryString=$queryString."&signature=".$signature;
        $url=$this->host . '/oauth2/login_access_token?' . $queryString;
        try {
            $rest_str=$this->_get($url);
            $rest=json_decode($rest_str,true);
            return $rest;
        } catch (\Exception $ex) {
            //throw $th;
            throw $ex;
        }
    }

    function get_login_user($access_token)  {
        $this->verify_base_parms();
        
        $headers = array(
            'access-token: '.$access_token,
            'client-id: '.$this->client_id
        ); 
       
        $url=$this->host . '/oauth2/user_info';
        try {
            $rest_str=$this->_get($url,$headers);
            $rest=json_decode($rest_str,true);
            return $rest;
        } catch (\Exception $ex) {
            //throw $th;
            throw $ex;
        }
    }


    function build_data_authorize($target_client_id,$redirect_uri,$response_type="code") : string {

        $this->verify_base_parms();
        $params = array(
            'client_id' => $this->client_id,
            'target_client_id' => $target_client_id,
            'redirect_uri' => $redirect_uri,
            'response_type'=>$response_type
        ); 
        $queryString = http_build_query($params);
        $signature=hash('sha256', $queryString.$this->secret);
        $queryString=$queryString."&signature=".$signature;
        return $this->host . '/oauth2/data_authorize?' . $queryString ;
    }

    function get_data_access_token($code)  {
        $this->verify_base_parms();
        $params = array(
            'client_id' => $this->client_id,
            'code' => $code
        ); 
        $queryString = http_build_query($params);
        $signature=hash('sha256', $queryString.$this->secret);
        $queryString=$queryString."&signature=".$signature;
        $url=$this->host . '/oauth2/data_access_token?' . $queryString;
        try {
            $rest_str=$this->_get($url);
            $rest=json_decode($rest_str,true);
            return $rest;
        } catch (\Exception $ex) {
            //throw $th;
            throw $ex;
        }
        return null;
    }

    function data_authorize_verify($access_token)  {
        $this->verify_base_parms();
        $headers = array(
            'access-token:'.$access_token,
            'client-id:'.$this->client_id
        ); 
        $url=$this->host . '/oauth2/data_authorize_verify';
        try {
            $rest_str=$this->_get($url,$headers);
            $rest=json_decode($rest_str,true);
            return $rest;
        } catch (\Exception $ex) {
            //throw $th;
            throw $ex;
        }
        return null;
    }

    function get_app_access_token($target_client_id)  {
        $this->verify_base_parms();
        $params = array(
            'client_id' => $this->client_id,
            'target_client_id' => $target_client_id
        ); 
        $queryString = http_build_query($params);
        $signature=hash('sha256', $queryString.$this->secret);
        $params["signature"]=$signature;

        $url=$this->host . '/oauth2/app_access_token';
        try {
            $rest_str=$this->_post_form($url,$params);
            $rest=json_decode($rest_str,true);
            return $rest;
        } catch (\Exception $ex) {
            //throw $th;
            throw $ex;
        }
        return null;
    }

    function app_authorize_verify($access_token)  {
        $this->verify_base_parms();
        $headers = array(
            'access-token:'.$access_token,
            'client-id:'.$this->client_id
        ); 
        $url=$this->host . '/oauth2/app_authorize_verify';
        try {
            $rest_str=$this->_get($url,$headers);
            $rest=json_decode($rest_str,true);
            return $rest;
        } catch (\Exception $ex) {
            //throw $th;
            throw $ex;
        }
        return null;
    }

    function user_register($username,$password,$nickname,$sex=1,$phone="",$email="",$head="")  {
        $this->verify_base_parms();
        $params = array(
            'client_id' => $this->client_id,
            'username' => $username,
            'password' => $password
        ); 
        $queryString = http_build_query($params);
        $signature=hash('sha256', $queryString.$this->secret);
        $params["signature"]=$signature;

        $params["nickname"]=$nickname;
        $params["sex"]=$sex;
        $params["phone"]=$phone;
        $params["email"]=$email;
        $params["head"]=$head;

        $url=$this->host . '/oauth2/do_register';
        try {
            $rest_str=$this->_post_form($url,$params);
            $rest=json_decode($rest_str,true);
            return $rest;
        } catch (\Exception $ex) {
            //throw $th;
            throw $ex;
        }
        return null;
    }

    function _get($url,$headers=null) : string {

        if($headers==null){
            $headers=array();
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => $this->timeout,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => $headers,
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    function _post_form($url,$data=null,$headers=null) : string {

        if($headers==null){
            $headers=array();
        }
        if($data==null){
            $data=array();
        }
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => $this->timeout,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }

    function _post_json($url,$data=null,$headers=null) : string {

        if($headers==null){
            $headers=array(
                'Content-Type: application/json'
            );
        }else{
            $headers[]='Content-Type: application/json';
        }
        if($data==null){
            $data=array();
        }
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => $this->timeout,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>json_encode($data),
        CURLOPT_HTTPHEADER =>$headers,
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }
}
?>